const express = require('express')

const router = express.Router()

const items = [
  { id: 1, name: 'Item 1' },
  { id: 2, name: 'Item 2' },
  { id: 3, name: 'Item 3' }
];

router.route('/').get((req, res) => {
  res.json(items.sort(
    (firstItem, secondItem) => firstItem.id - secondItem.id)
  );
})

router.route('/:id').get((req, res) => {
  const id = parseInt(req.params.id);
  const item = items.find((item) => item.id === id);

  if (item) {
    res.json(item);
  } else {
    res.status(404).json({ error: 'Item not found' });
  }
})

router.route('/').post((req, res) => {
  const newItem = req.body;
  items.push(newItem);
  res.status(201).json(newItem);
})

router.route('/:id').put((req, res) => {
  const id = parseInt(req.params.id);
  const updatedItem = req.body;
  const index = items.findIndex((item) => item.id === id);

  if (index !== -1) {
    items[index] = { ...items[index], ...updatedItem };
    res.json(items[index]);
  } else {
    res.status(404).json({ error: 'Item not found' });
  }
})

router.route('/:id').delete((req, res) => {
  const id = parseInt(req.params.id);
  const index = items.findIndex((item) => item.id === id);

  if (index !== -1) {
    const deletedItem = items[index];
    items.splice(index, 1);
    res.json(deletedItem);
  } else {
    res.status(404).json({ error: 'Item not found' });
  }
})

module.exports = router