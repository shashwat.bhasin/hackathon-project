const express = require('express')

const router = express.Router()

const users = [
  { id: 1, name: 'User 1' },
  { id: 2, name: 'User 2' },
  { id: 3, name: 'User 3' }
];

router.route('/').get((req, res) => {
  res.json(users.sort(
    (firstUser, secondUser) => firstUser.id - secondUser.id)
  );
})

router.route('/:id').get((req, res) => {
  const id = parseInt(req.params.id);
  const user = users.find((user) => user.id === id);

  if (user) {
    res.json(user);
  } else {
    res.status(404).json({ error: 'User not found' });
  }
})

router.route('/').post((req, res) => {
  const newUser = req.body;
  users.push(newUser);
  res.status(201).json(newUser);
})

router.route('/:id').put((req, res) => {
  const id = parseInt(req.params.id);
  const updatedUser = req.body;
  const index = users.findIndex((user) => user.id === id);

  if (index !== -1) {
    users[index] = { ...users[index], ...updatedUser };
    res.json(users[index]);
  } else {
    res.status(404).json({ error: 'User not found' });
  }
})

router.route('/:id').delete((req, res) => {
  const id = parseInt(req.params.id);
  const index = users.findIndex((user) => user.id === id);

  if (index !== -1) {
    const deletedUser = users[index];
    users.splice(index, 1);
    res.json(deletedUser);
  } else {
    res.status(404).json({ error: 'User not found' });
  }
})

module.exports = router