const express = require('express');
const bodyParser = require('body-parser');
const itemsRoutes = require('./routes/itemsRoutes')
const usersRoutes = require('./routes/usersRoutes')

const app = express();

app.use(bodyParser.json());

app.use('/items', itemsRoutes)
app.use('/users', usersRoutes)

app.listen(port = 3000, () => {
  console.log(`Server is running on port ${port}`);
});

